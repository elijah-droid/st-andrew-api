from django.db import models
from django.utils.timezone import now

class Admission(models.Model):
    date = models.DateTimeField(default=now)
    student_admitted = models.OneToOneField('Students.Student', models.CASCADE)
    admitted_by = models.ForeignKey('Staff.Staff', models.CASCADE)

