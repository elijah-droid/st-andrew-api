from django.urls import path
from .views import admit_student, view_admissions


urlpatterns = [
    path('admit/<int:student>/', admit_student, name='admit-student'),
    path('', view_admissions, name='view-admissions')
]