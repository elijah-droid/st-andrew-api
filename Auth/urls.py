from django.urls import path
from .views import login_user, create_user, update_user_info, user_profile, set_password


urlpatterns = [
    path('login/user/', login_user, name='login-user'),
    path('create-user/', create_user, name='create-user'),
    path('update-user-info/<int:user>/', update_user_info, name='update-user'),
    path('user-profile/<int:user>/', user_profile, name='user-profile'),
    path('set-password/<int:user>/', set_password, name='set-password')
]