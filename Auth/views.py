from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse
import json
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate
from .models import ApiKey
import uuid


def login_user(request):
    body = json.loads(request.body)
    if request.method == 'POST':
        user = authenticate(request, username=body['username'], password=body['password'])
        if user is not None:
            try:
                key = ApiKey.objects.get(user=user)
                key.key = uuid.uuid4()
                key.save()
            except ApiKey.DoesNotExist:
                key = ApiKey.objects.create(user=user)
            data = {'message': 'Logged in successfully', 'token': key.key, 'userid': user.id}
            status = 200
        else:
            data = {'message': 'Invalid Login Credentials'}
            status = 401
    else:
        data = {'error': 'method not allowed'}
        status = 401
    return JsonResponse(data, status=status)


def create_user(request):
    body = json.loads(request.body)
    if request.method == 'POST':
        user = User.objects.create(**body)
        data = {'message': 'user created successfully', 'userid': user.id}
        status = 200
    else:
        data = {'error': 'method not allowed'}
        status=401
    return JsonResponse(data, status=status)


def update_user_info(request, user):
    body = json.loads(request.body)
    user = User.objects.filter(id=user)
    if request.method == 'POST':
        user.update(**body)
        data = {'message': f'{user[0]} updated successfully'}
        status = 200
    else:
        data = {'error': 'method not allowed'}
        status = 401
    return JsonResponse(data, status=status)


def user_profile(request, user):
    user = User.objects.filter(id=user)
    serialized_data = serialize('json', user)
    return JsonResponse(json.loads(serialized_data)[0], safe=False)


def set_password(request, user):
    user = User.objects.filter(id=user)
    body = json.loads(request.body)
    if request.method == 'POST':
        user.update(password=make_password(body['password']))
        data = {'message': f'{user[0]} password updated successfully'}
        status = 200
    else:
        data = {'error': 'method not allowed'}
        status = 401
    return JsonResponse(data, status=status)