from django.db import models

class Class(models.Model):
    name = models.CharField(max_length=100)
    students = models.ManyToManyField('Students.Student')
    Teachers = models.ManyToManyField('Teachers.Teacher')

    def __str__(self):
        return self.name