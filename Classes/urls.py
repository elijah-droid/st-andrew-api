from django.urls import path
from .views import view_classes, add_class, class_profile, edit_class_info, add_student_to_class

urlpatterns = [
    path('', view_classes, name='view-classes'),
    path('add/', add_class, name='add-class'),
    path('profile/<int:class>/', class_profile, name='class-profile'),
    path('edit-info/<int:class>/', edit_class_info, name='edit-class-info'),
    path('<int:class_>/add-student/<int:student>/', add_student_to_class, name='add-student-to-class')
]