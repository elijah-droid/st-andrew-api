from django.shortcuts import render
from django.http import JsonResponse
import json
from .models import Class
from Students.models import Student


def add_class(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        class_ = Class.objects.create(**body)
        data = {'message': f'{class_} Created Successfully', 'classid': class_.id}
        status = 200
    else:
        data = {'error': 'method not allowed'}
        status = 401
    return JsonResponse(data, status=status)


def add_student_to_class(request, student, class_):
    student = Student.objects.get(id=student)
    class_ = Class.objects.get(id=class_)
    class_.students.add(student)
    student.student_class = class_
    student.save()
    data = {'message': f'{student} added to {class_} Successfully'}
    status = 200
    return JsonResponse(data, status=status)


def view_classes(request):
    pass


def class_profile(request):
    pass


def edit_class_info(request):
    pass