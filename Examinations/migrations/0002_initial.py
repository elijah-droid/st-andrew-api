# Generated by Django 5.0.3 on 2024-03-27 16:30

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Examinations', '0001_initial'),
        ('Reports', '0001_initial'),
        ('Terms', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='examination',
            name='reports',
            field=models.ManyToManyField(to='Reports.report'),
        ),
        migrations.AddField(
            model_name='examination',
            name='term',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='Terms.term'),
        ),
    ]
