from django.db import models

class Examination(models.Model):
    name = models.CharField(max_length=100)
    term = models.ForeignKey('Terms.Term', models.CASCADE)
    reports = models.ManyToManyField('Reports.Report')
