from django.urls import path
from .views import view_examinations, set_examination, reschedule_examination


urlpatterns = [
    path('', view_examinations, name='view-examinations'),
    path('set/', set_examination, name='set-examinations'),
    path('reschedule/<int:examination>/', reschedule_examination, name='reschedule-examination')
]