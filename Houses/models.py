from django.db import models

class House(models.Model):
    name = models.CharField(max_length=100)
    students = models.ManyToManyField('Students.Student')
    patron = models.ForeignKey('Staff.Staff', models.SET_NULL, null=True)
