from django.urls import path
from .views import create_house, view_houses, house_profile, add_student_to_house, edit_house_info


urlpatterns = [
    path('create/', create_house, name='create-house'),
    path('', view_houses, name='view-houses'),
    path('profile/<int:house>/', house_profile, name='house-profile'),
    path('<int:house>/add-student/<int:student>/', add_student_to_house, name='add-student-to-house'),
    path('edit-info/<int:house>/', edit_house_info, name='edit-house-info')
]