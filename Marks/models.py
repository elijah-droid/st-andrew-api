from django.db import models
from django.contrib.auth.models import User

class Mark(models.Model):
    exam = models.ForeignKey('Examinations.Examination', models.CASCADE)
    student = models.ForeignKey('Students.Student', models.CASCADE)
    subject = models.ForeignKey('Subjects.Subject', models.CASCADE)
    score = models.IntegerField()
    awarded_by = models.ForeignKey(User, models.CASCADE)