from django.urls import path
from .views import edit_marks, view_marks


urlpatterns = [
    path('edit/<int:exam>/<int:class_>/', edit_marks, name='edit-marks'),
    path('<int:exam>/<int:class_>/', view_marks, name='view-marks')
]