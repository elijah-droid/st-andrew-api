from django.db import models
from django.contrib.auth.models import User

class Parent(models.Model):
    user = models.OneToOneField(User, models.CASCADE)
    children = models.ManyToManyField('Students.Student')
