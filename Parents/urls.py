from django.urls import path
from .views import view_reports


urlpatterns = [
    path('view-reports/', view_reports, name='parent-reports')
]