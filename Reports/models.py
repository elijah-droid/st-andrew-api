from django.db import models

class Report(models.Model):
    student = models.ForeignKey('Students.Student', models.CASCADE)
    exam = models.ForeignKey('Examinations.Examination', models.CASCADE)
    marks = models.ManyToManyField('Marks.Mark')
    aggregate_or_points = models.IntegerField()
    position = models.IntegerField()
    

