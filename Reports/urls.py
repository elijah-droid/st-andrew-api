from django.urls import path
from .views import view_reports, export_reports


urlpatterns = [
    path('<int:class_>/<int:term>/<int:examination>/', view_reports, name='view-reports'),
    path('export/<int:class_>/<int:term>/<int:examination>/<format>/', export_reports, name='export-reports')
]