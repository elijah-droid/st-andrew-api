from django.db import models

roles = (
    ('Headmaster', 'Headmaster'),
)

class Staff(models.Model):
    role = models.CharField(max_length=100, choices=roles)
    