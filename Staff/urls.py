from django.urls import path
from .views import register_staff


urlpatterns = [
    path('register/<int:user>/', register_staff, name='register-staff')
]