from django.contrib import admin
from Students.models import Student


class StudentAdmin(admin.ModelAdmin):
    list_display = ('user', 'student_id')


admin.site.register(Student, StudentAdmin)