from django.db import models
from django.contrib.auth.models import User
import random


def generate_studentid():
    id_no = random.randint(0, 999)
    id = 'ST#'+str(id_no)
    return id

class Student(models.Model):
    user = models.OneToOneField(User, models.CASCADE)
    student_id = models.CharField(blank=True, max_length=100)
    student_admission = models.ForeignKey('Admissions.Admission', models.SET_NULL, null=True)
    student_class = models.ForeignKey('Classes.Class', models.CASCADE, null=True)
    stream = models.ForeignKey('Streams.Stream', models.SET_NULL, null=True)
    parents = models.ManyToManyField('Parents.Parent', blank=True)
    subjects = models.ManyToManyField('Subjects.Subject')

    def __str__(self):
        return str(self.user)