from django.urls import path
from .views import register_student, view_students, student_profile, edit_student_info


urlpatterns = [
    path('register/<int:user>/', register_student, name='register-student'),
    path('<int:class_>/', view_students, name='view-students'),
    path('profile/<int:student>/', student_profile, name='student-profile'),
    path('<int:student>/edit-info/', edit_student_info, name='edit-info')
]