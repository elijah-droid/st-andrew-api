from django.shortcuts import render
from django.http import JsonResponse
from .models import Student
from django.contrib.auth.models import User
from django.core.serializers import serialize
import json

def register_student(request, user):
    user = User.objects.get(id=user)
    body = json.loads(request.body)
    if request.method == 'POST':
        student = Student.objects.create(
            user=user,
            **body
        )
        data = {'message': 'student registered successfully', 'student_id': student.student_id}
        status = 200
    else:
        data = {'error': 'Method not allowed'}
        status = 401
    return JsonResponse(data, status=status)


def view_students(request, class_):
    students = Student.objects.all()
    serialized_data = serialize('json', students)
    return JsonResponse(json.loads(serialized_data), safe=False)


def student_profile(request, student):
    pass


def edit_student_info(request, student):
    pass