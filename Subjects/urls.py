from django.urls import path
from .views import add_subject, view_subjects


urlpatterns = [
    path('add/', add_subject, name='add-subject'),
    path('', view_subjects, name='view-subjects'),
]