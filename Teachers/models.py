from django.db import models
from django.contrib.auth.models import User


class Teacher(models.Model):
    user = models.OneToOneField(User, models.CASCADE)
    school_work = models.ManyToManyField('Works.Work', related_name="teacher_s_work")
    subjects = models.ManyToManyField('Subjects.Subject')
    classes = models.ManyToManyField('Classes.Class', through='Works.Work')
    timetables = models.ManyToManyField('Works.TimeTable')