from .views import register_teacher, view_teachers, teachers_profile
from django.urls import path


urlpatterns = [
    path('register/<int:user>/', register_teacher, name='register-teacher'),
    path('', view_teachers, name='view-teachers'),
    path('teachers_profile/<int:teacher>/', teachers_profile, name='teachers-profile')
]