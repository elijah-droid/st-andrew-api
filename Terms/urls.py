from django.urls import path
from .views import create_term, view_terms


urlpatterns = [
    path('create/', create_term, name='create-term'),
    path('', view_terms, name='view-terms')
]