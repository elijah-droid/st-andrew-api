from django.contrib import admin
from .models import Work


admin.site.site_title = 'St. Andrew SS Admin Panel'
admin.site.site_header = 'St. Andrew SS Admin Panel'


admin.site.register(Work)