from django.db import models


days = (
    ('Monday', 'Monday'),
    ('Tuesday', 'Tuesday'),
    ('Wednesday', 'Wednesday'),
    ('Thursday', 'Friday'),
    ('Friday', 'Friday'),
    ('Saturday', 'Saturday'),
    ('Sunday', 'Sunday')
)

class Work(models.Model):
    teacher = models.ForeignKey('Teachers.Teacher', models.CASCADE)
    teaching_class = models.ForeignKey('Classes.Class', models.CASCADE)
    subjects = models.ManyToManyField('Subjects.Subject')


class TimeTable(models.Model):
    Teacher = models.ForeignKey('Teachers.Teacher', models.CASCADE, related_name="work_of_teacher")
    Class = models.ForeignKey('Classes.Class', models.CASCADE)
    Day = models.CharField(max_length=100, choices=days)
    From = models.TimeField()
    To = models.TimeField()


    class Meta:
        unique_together = ('Teacher', 'Class', 'Day', 'From')

    