from .views import add_work
from django.urls import path


urlpatterns = [
    path('add/', add_work, name='add-work'),
]