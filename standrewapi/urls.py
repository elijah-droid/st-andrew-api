from django.contrib import admin
from django.urls import path, include
from .views import index, land, dashboard

urlpatterns = [
    path('', land, name='landing-page'),
    path('api/<url>/', index),
    path('api/admissions/', include('Admissions.urls')),
    path('api/dasboard/', dashboard, name='dashboard'),
    path('api/auth/', include('Auth.urls')),
    path('api/classes/', include('Classes.urls')),
    path('api/examinations/', include('Examinations.urls')),
    path('api/houses/', include('Houses.urls')),
    path('api/marks/', include('Marks.urls')),
    path('api/parents/', include('Parents.urls')),
    path('api/reports/', include('Reports.urls')),
    path('api/staff/', include('Staff.urls')),
    path('api/students/', include('Students.urls')),
    path('api/subjects/', include('Subjects.urls')),
    path('api/teachers/', include('Teachers.urls')),
    path('api/Terms/', include('Terms.urls')),
    path('api/work/', include('Works.urls')),
    path('admin/', admin.site.urls),
]
