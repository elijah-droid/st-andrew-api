import requests
from django.http import JsonResponse, HttpResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
import json
from Parents.models import Parent
from Students.models import Student
from Teachers.models import Teacher

@csrf_exempt
def index(request, url):
    body = json.loads(request.body)
    reversed_url = 'https://api.standrewkaggwakabimbiri.ac'+reverse(url, kwargs=body['params'])

    if request.method == 'POST':
        try:
            response = requests.post(reversed_url, json=body['data'])
            return JsonResponse(response.json())
        except requests.RequestException as e:
            return JsonResponse({'error': f'Failed to send POST request: {str(e)}'}, status=500)
    else:
        try:
            response = requests.get(reversed_url)
            return JsonResponse(response.json(), status=response.status_code, safe=False)
        except requests.RequestException as e:
            return JsonResponse({'error': f'Failed to send GET request: {str(e)}'}, status=500)

def land(request):
    return HttpResponse('St. Andrew API V1')


def dashboard(request):
    parents = Parent.objects.count()
    reports_generated = 0
    students = Student.objects.count()
    teachers = Teacher.objects.count()
    data = {
        'parents': parents,
        'reports_generated': reports_generated,
        'students': students,
        'teachers': teachers
    }
    return JsonResponse(data)